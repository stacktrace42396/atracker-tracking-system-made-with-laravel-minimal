<?php

namespace AttendanceTracker;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
 	protected $table = 'users';

 	public function Records(){

 		return $this->hasMany('AttendanceTracker\Records');
 	}
}
