<?php

namespace AttendanceTracker\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if($user->privilege == 'Admin'){
            return view('adminhome');
        }
        else if($user->privilege == 'Master'){
            return view('masterhome');
        }
        else{
            return view('clienthome');
        }
    }
}
