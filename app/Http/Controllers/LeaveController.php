<?php

namespace AttendanceTracker\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use AttendanceTracker\Records;
use Image;
use Auth;

class LeaveController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

    public function index(){

    	return view('leave.leaveform');
    }

    public function set(Request $request){

    	$validator = Validator::make($request->all(), [
    			'datefrom'=>'required',
    			'dateto'=>'required',
                'image_map'=>'required',
                'lat'=>'required',
                'lon'=>'required'
    		]);

    	if($validator->validate()){

    		return redirect()->to('/leave')->with('errors', $validator);
    	}
    	else{

    		if($request->input('datefrom') < date("Y-m-d")){

    			return redirect()->to('/leave')->with('error', 'Date Must Be Present or Future From');
    		}
    		else if($request->input('dateto') <= date("Y-m-d")){

    			return redirect()->to('/leave')->with('error', 'Date Must Be on the Future To');
    		}
    		else{

    			for($datefrom = strtotime($request->input('datefrom')); $datefrom <= strtotime($request->input('dateto')); $datefrom = strtotime('+1 day', $datefrom)){

    					if(date('l', $datefrom) == 'Sunday' || date('l', $datefrom) == 'Saturday'){
    						//	do nothing
    					}
    					else{

                            $image_map = $request->input('image_map');
                            $filename = 'timeinlate-' . time() . '.' . 'png';
                            Image::make($image_map)->resize(750,750)->save( public_path('/uploads/locations/' . $filename) );

    						$records = new Records;
    						$records->login = null;
    						$records->logout = null;
    						$records->statuslog = 'on leave';
                            $records->leavelog = date('Y-m-d H:m:s', $datefrom);
    						$records->user_id = Auth::user()->id;
    						$records->created_at = date('Y-m-d H:m:s', $datefrom);
    						$records->updated_at = date('Y-m-d H:m:s');
                            $records->image_map = $filename;
                            $records->lat = $request->input('lat');
                            $records->lon = $request->input('lon');
    						$records->save();
    					}

    			}
    			return redirect()->to('/leave')->with('success', 'Added On Leave on Records');
    		}
    	}

    }
}
