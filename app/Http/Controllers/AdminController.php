<?php

namespace AttendanceTracker\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use AttendanceTracker\Users;
use Image;
use Auth;

class AdminController extends Controller
{
	public function __construct(){
		$this->middleware('auth');
	}

    public function ShowAccounts(){

    	if(Auth::user()->privilege == 'Admin'){

    		$users = Users::where('position', Auth::user()->position)
                ->where('id', '!=', Auth::user()->id)->orderBy('id', 'desc')->paginate(15);

    		return view('admin.allaccounts', ['users'=>$users]);

    	}
    	else{

    		return redirect()->to('/home');

    	}
    }

    public function ViewAccount($id){

        if(Auth::user()->privilege == 'Admin'){

            $user = Users::find($id);

            return view('admin.viewaccount', ['user'=>$user]);

        }
        else{

            return redirect()->to('/home');

        }
    }

    public function EditAccount(){

        if(Auth::user()->privilege == 'Admin'){

            $pos = array('','Project Manager','Business Admin','Systems Admin',
                'Software Engineer','Quality Tester');

            $priv = array('','Admin','Client');

            return view('admin.editaccount', ['pos'=>$pos]);

        }
        else{

            return redirect()->to('/home');

        }
    }

    public function validator(array $data){

        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.Auth::user()->id.'',
            'position' => 'required',
            'privilege' => 'required'
        ]);

    }

    public function UpdateAccount(Request $request){

        if(Auth::user()->privilege == 'Admin'){

            if($validator = $this->validator($request->all())->validate()){
                
                return redirect()->route('editadminaccount')->with('errors', $validator);   
            }
            else{

                if($request->hasFile('avatar')){
                    $avatar = $request->file('avatar');
                    $filename = time() . '.' . $avatar->getClientOriginalExtension();
                    Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/' . $filename) );
                    $user = Auth::user();
                    $user->avatar = $filename;
                    $user->name = $request->input('name');
                    $user->email = $request->input('email');
                    $user->position = $request->input('position');
                    $user->privilege = $request->input('privilege');
                    $user->save();

                    return redirect()->route('editadminaccount')
                            ->with('success','account updated');
                }
                else{
                    $user = Auth::user();
                    $user->name = $request->input('name');
                    $user->email = $request->input('email');
                    $user->position = $request->input('position');
                    $user->privilege = $request->input('privilege');
                    $user->save();

                    return redirect()->route('editadminaccount')
                            ->with('success','account updated');
                }

            }
        }
        else{

            return redirect()->to('/home');

        }
    }
}
