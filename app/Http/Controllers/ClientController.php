<?php

namespace AttendanceTracker\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Image;
use Auth;

class ClientController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

    public function ViewEditAccount(){

    	if(Auth::user()->privilege = 'Client'){

    		$pos = array('','Project Manager','Business Admin','Systems Admin',
                'Software Engineer','Quality Tester');
            
            return view('client.editaccount', ['pos'=>$pos]);

    	}
    	else{
    		
            return redirect()->route('/home');

    	}
    }

    public function validator(array $data){

        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.Auth::user()->id.'',
            'position' => 'required',
            'privilege' => 'required'
        ]);

    }

    public function EditAccount(Request $request){

    	if(Auth::user()->privilege = 'Client'){

    		if($validator = $this->validator($request->all())->validate()){
                
                return redirect()->route('vieweditclient')->with('errors', $validator);   
            }
            else{

                if($request->hasFile('avatar')){
                    $avatar = $request->file('avatar');
                    $filename = time() . '.' . $avatar->getClientOriginalExtension();
                    Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/' . $filename) );
                    $user = Auth::user();
                    $user->avatar = $filename;
                    $user->name = $request->input('name');
                    $user->email = $request->input('email');
                    $user->position = $request->input('position');
                    $user->privilege = $request->input('privilege');
                    $user->save();

                    return redirect()->route('vieweditclient')
                            ->with('success','account updated');
                }
                else{
                    $user = Auth::user();
                    $user->name = $request->input('name');
                    $user->email = $request->input('email');
                    $user->position = $request->input('position');
                    $user->privilege = $request->input('privilege');
                    $user->save();

                    return redirect()->route('vieweditclient')
                            ->with('success','account updated');
                }

            }
    		
    	}
    	else{
    		
    		return redirect()->to('/home');

    	}
    		
    }
}
