<?php

namespace AttendanceTracker\Http\Controllers;

use Illuminate\Http\Request;
use AttendanceTracker\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use AttendanceTracker\Records;
use AttendanceTracker\Users;
use Image;
use Auth;


class MasterController extends Controller
{

    public function __construct(){
    	$this->middleware('auth');
    }

    public function ShowAccounts(){

    	if(Auth::user()->privilege == 'Master'){

    		$users = Users::where('privilege','Admin')
    						->orWhere('privilege','Client')
                            ->orderBy('id', 'desc')
    						->paginate(15);

    		return view('master.allaccounts', ['users'=>$users]);

    	}
    	else{
    		
    		return redirect()->to('/home');

    	}
    }

    public function ViewEditAccount($id){

        if(Auth::user()->privilege == 'Master'){

            $user = Users::find($id);

            $pos = array('','Project Manager','Business Admin','Systems Admin',
                'Software Engineer','Quality Tester');

            $priv = array('','Admin','Client');

            return view('master.editaccount', ['user'=>$user, 'pos'=>$pos, 'priv'=>$priv]);

        }

        else{

            return redirect()->to('/home');
        }
    }

    public function validator(array $data, $id){
        
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$id.'',
            'position' => 'required',
            'privilege' => 'required'
        ]);

    }

    public function EditAccount($id, Request $request){

        if(Auth::user()->privilege == 'Master'){
            
            if($validator = $this->validator($request->all(), $id)->validate()){
                return redirect()->route('editaccount', $id, $validator);
            }
            else{

                if($request->hasFile('avatar')){
                    $avatar = $request->file('avatar');
                    $filename = time() . '.' . $avatar->getClientOriginalExtension();
                    Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/' . $filename) );
                    $user = Users::find($id);
                    $user->avatar = $filename;
                    $user->name = $request->input('name');
                    $user->email = $request->input('email');
                    $user->position = $request->input('position');
                    $user->privilege = $request->input('privilege');
                    $user->save();

                    return redirect()->route('editaccount', $id)->with('success','Account Successfully Updated');
                }
                
                else{

                    $user = Users::find($id);

                    $user->name = $request->input('name');
                    $user->email = $request->input('email');
                    $user->position = $request->input('position');
                    $user->privilege = $request->input('privilege');

                    $user->save();

                    return redirect()->route('editaccount', $id)->with('success','Account Successfully Updated');

                }
                
            }
                       
        }

        else{

            return redirect()->to('/home');

        }
    }

    public function ViewDeleteAccount($id){

        if(Auth::user()->privilege == 'Master'){

            $user = Users::find($id);

            return view('master.deleteaccount', ['user'=>$user]);

        }

        else{

            return redirect()->to('/home');

        }

    }

    public function DeleteAccount($id){

        if(Auth::user()->privilege == 'Master'){

            $user = Users::find($id)->delete();

            return redirect()->route('accountmaster')->with('success','Account Deleted');

        }

        else{

            return redirect()->to('/home');

        }
    }

    public function ShowRecords(){

        if(Auth::user()->privilege == 'Master'){

            $records = Records::with('Users')->orderBy('id', 'desc')->paginate(15);

            return view('master.allattendance', ['records'=>$records]);

        }
        else{

            return redirect()->to('/home');
            
        }
    }

    public function ShowEditRecords($id){

        if(Auth::user()->privilege == 'Master'){

            $record = Records::find($id);
            $status = array('','late:office','present:office','timed out','work from home','on meeting','absent','on leave');

            return view('master.editattendance', ['record'=>$record, 'status'=>$status]);
        }
        else{

            return redirect()->to('/home');
        }
    }

    public function SetEditRecords($id, Request $request){

        if(Auth::user()->privilege == 'Master'){


            $validate = Validator::make($request->all(), [
                    'status'=>'required'
                ]);

            if($validate->validate()){

                return redirect()->route('showeditrecord', $id, $validate);
            }
            else{

                $record = Records::find($id);
                $record->statuslog = $request->input('status');
                $record->login = $request->input('login');
                $record->logout = $request->input('logout');
                $record->updated_at = date('Y-m-d H:m:s');
                $record->save();

                return redirect()->route('showeditrecord', $id)->with('success', 'Record Successfully Updated');   
            }
        }
        else{

            return redirect()->to('/home');
        }
    }

    public function ShowDeleteRecord($id){

        if(Auth::user()->privilege == 'Master'){

            $record = Records::find($id);

            return view('master.deleteattendance', ['record'=>$record]);
        }
        else{

            return redirect()->to('/home');
        }
    }

    public function SetDeleteRecord($id){

        if(Auth::user()->privilege == 'Master'){

            $record = Records::find($id)->delete();

            return redirect()->route('showallattendance')->with('success', 'Record Deleted');
        }
        else{

            return redirect()->to('/home');
        }
    }

    public function SortRecords(Request $request){

        if(Auth::user()->privilege == 'Master'){

           if($request->input('search') != ""){

            $search = $request->input('search');

                if($request->input('recordset') == 'year'){

                    $records = Records::whereHas('Users', function($query) use($search){
                        $query->where('name', 'like', '%'.$search.'%');
                        })->whereYear('created_at', '=', date('Y'))
                        ->paginate(15);

                    if($records->isEmpty()){
                        return redirect()->route('showallattendance')->with('error', 'No Results Found');    
                    }
                    else{
                        return view('master.allattendance', ['records'=>$records]);
                    }
                
                    //return view('master.allattendance', ['records'=>$records]);
                    //return redirect()->route('showallattendance')->with('records', $records);
                }
                else if($request->input('recordset') == 'month'){

                    $records = Records::whereHas('Users', function($query) use($search){
                        $query->where('name', 'like', '%'.$search.'%');
                    })->whereMonth('created_at','=', date('m'))->paginate(15);

                    if($records->isEmpty()){
                        return redirect()->route('showallattendance')->with('error', 'No Results Found');    
                    }
                    else{
                        return view('master.allattendance', ['records'=>$records]);
                    }

                    //return view('master.allattendance', ['records'=>$records]);
                    //return redirect()->route('showallattendance')->with('records', $records);
                }
                else{

                    $records = Records::whereHas('Users', function($query) use($search){
                        $query->where('name', 'like', '%'.$search.'%');
                    })->whereDate('created_at', '=', date('Y-m-d'))->paginate(15);

                    if($records->isEmpty()){
                        return redirect()->route('showallattendance')->with('error', 'No Results Found');    
                    }
                    else{
                        return view('master.allattendance', ['records'=>$records]);
                    }

                    //return view('master.allattendance', ['records'=>$records]);
                    //return redirect()->route('showallattendance')->with('records', $records);
                }
           }
           else{

                if($request->input('recordset') == 'year'){

                    $records = Records::where('user_id','!=',Auth::user()->id)->whereYear('created_at', '=', date('Y'))->paginate(15);
                
                    return view('master.allattendance', ['records'=>$records]);
                    //return redirect()->route('showallattendance', ['records'=>$records]);
                }
                else if($request->input('recordset') == 'month'){

                    $records = Records::where('user_id','!=',Auth::user()->id)->whereMonth('created_at','=', date('m'))->paginate(15);

                    return view('master.allattendance', ['records'=>$records]);
                    //return redirect()->route('showallattendance', ['records'=>$records]);
                }
                else{

                    $records = Records::where('user_id','!=',Auth::user()->id)->whereDate('created_at', '=', date('Y-m-d'))->paginate(15);

                    return view('master.allattendance', ['records'=>$records]);
                    //return redirect()->route('showallattendance', ['records'=>$records]);
                }
            }
        }
        else{

            return redirect()->to('/home');
        }
    }
}
