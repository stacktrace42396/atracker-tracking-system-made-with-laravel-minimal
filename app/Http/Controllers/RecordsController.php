<?php

namespace AttendanceTracker\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use AttendanceTracker\Records;
use Illuminate\Http\Request;
use AttendanceTracker\Users;
use Image;
use Auth;

class RecordsController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

    public function index(){

        $records = Records::with('Users')->where('user_id','=',Auth::user()->id)->orderBy('id', 'desc')->paginate(15);

    	return view('records.myrecords', ['records' => $records]);
    }

    public function set(Request $request){

        $validation = Validator::make($request->all(), [
                    'image_map' => 'required',
                    'lat'=>'required',
                    'lon'=>'required'
            ]);

        if($validation->validate()){

            return redirect()->to('/home')->with('errors', $validator);
        }
    	else if($request->has('timein')){

            $result = Records::where('user_id','=',Auth::user()->id)->whereDate('created_at','=',date('Y-m-d'))->get();
            
            if(count($result) <= 0){

                if( date('H', time()) > 9 ){
                    
                    $image_map = $request->input('image_map');
                    $filename = 'timeinlate-' . time() . '.' . 'png';
                    Image::make($image_map)->resize(750,750)->save( public_path('/uploads/locations/' . $filename) );

                    $records = new Records;
                    $records->login = date('Y-m-d H:m:s');
                    $records->logout = null;
                    $records->statuslog = 'late:office';
                    $records->leavelog = null;
                    $records->user_id = Auth::user()->id;
                    $records->created_at = date('Y-m-d H:m:s');
                    $records->updated_at = date('Y-m-d H:m:s');
                    $records->image_map = $filename;
                    $records->lat = $request->input('lat');
                    $records->lon = $request->input('lon');
                    $records->save();               
                    
                    return redirect()->to('/records')->with('success', 'Record Late Added');  
                }
                else{

                    $image_map = $request->input('image_map');
                    $filename = 'timeinpresent-' . time() . '.' . 'png';
                    Image::make($image_map)->resize(750,750)->save( public_path('/uploads/locations/' . $filename) );

                    $records = new Records;
                    $records->login = date('Y-m-d H:m:s');
                    $records->logout = null;
                    $records->statuslog = 'present:office';
                    $records->leavelog = null;
                    $records->user_id = Auth::user()->id;
                    $records->created_at = date('Y-m-d H:m:s');
                    $records->updated_at = date('Y-m-d H:m:s');
                    $records->image_map = $filename;
                    $records->lat = $request->input('lat');
                    $records->lon = $request->input('lon');
                    $records->save();

                    return redirect()->to('/records')->with('success', 'Record Time In Added');
                }

                
            }
            else{

                return redirect()->to('/records')->with('error', 'Already Logged On This Day');   
            }
    	}
    	else if($request->has('timeout')){

            $result = Records::where('user_id','=',Auth::user()->id)
                    ->whereDate('created_at','=',date('Y-m-d'))
                    ->where('logout','!=',date('Y-m-d'))->get();

            if(count($result) <= 0){

                if( date('H', time()) < 17 ){

                    return redirect()->to('/records')->with('error', 'Working Time on going');       
                }
                else{

                    $image_map = $request->input('image_map');
                    $filename = 'timeout-' . time() . '.' . 'png';
                    Image::make($image_map)->resize(750,750)->save( public_path('/uploads/locations/' . $filename) );

                    $user = Auth::user();
                    $records = Records::where('user_id','=',$user->id)->whereDate('created_at','=',date('Y-m-d'))->first();

                    $records->logout = date('Y-m-d H:m:s');
                    $records->statuslog = 'timed out';
                    $records->updated_at = date('Y-m-d H:m:s');
                    $records->image_map = $filename;
                    $records->lat = $request->input('lat');
                    $records->lon = $request->input('lon');
                    $records->save();

                    return redirect()->to('/records')->with('success', 'Successfully Timed Out');   
                }

            }
            else{

                return redirect()->to('/records')->with('error', 'Already Timed Out This Day');   
            }
    	}
    	else if($request->has('workfromhome')){

            $result = Records::where('user_id','=',Auth::user()->id)->whereDate('created_at','=',date('Y-m-d'))->get();

            if(count($result) <= 0){

                $image_map = $request->input('image_map');
                $filename = 'workfromhome-' . time() . '.' . 'png';
                Image::make($image_map)->resize(750,750)->save( public_path('/uploads/locations/' . $filename) );

                $records = new Records;
                $records->login = date('Y-m-d H:m:s');
                $records->logout = null;
                $records->statuslog = 'work from home';
                $records->leavelog = null;
                $records->user_id = Auth::user()->id;
                $records->created_at = date('Y-m-d H:m:s');
                $records->updated_at = date('Y-m-d H:m:s');
                $records->image_map = $filename;
                $records->lat = $request->input('lat');
                $records->lon = $request->input('lon');
                $records->save();

                return redirect()->to('/records')->with('success', 'Record Work From Home Added');

            }
            else{

                $image_map = $request->input('image_map');
                $filename = 'workfromhome-' . time() . '.' . 'png';
                Image::make($image_map)->resize(750,750)->save( public_path('/uploads/locations/' . $filename) );

                $user = Auth::user();
                $records = Records::where('user_id','=',$user->id)->whereDate('created_at','=',date('Y-m-d'))->first();

                $records->statuslog = 'work from home';
                $records->updated_at = date('Y-m-d H:m:s');
                $records->image_map = $filename;
                $records->lat = $request->input('lat');
                $records->lon = $request->input('lon');
                $records->save();

                return redirect()->to('/records')->with('success', 'Record Updated Work From Home');  
            }
                
    	}
    	else if($request->has('onmeeting')){

            $result = Records::where('user_id','=',Auth::user()->id)->whereDate('created_at','=',date('Y-m-d'))->get();

            if(count($result) <= 0){

                $image_map = $request->input('image_map');
                $filename = 'onmeeting-' . time() . '.' . 'png';
                Image::make($image_map)->resize(750,750)->save( public_path('/uploads/locations/' . $filename) );

                $records = new Records;
                $records->login = date('Y-m-d H:m:s');
                $records->logout = null;
                $records->statuslog = 'on meeting';
                $records->leavelog = null;
                $records->user_id = Auth::user()->id;
                $records->created_at = date('Y-m-d H:m:s');
                $records->updated_at = date('Y-m-d H:m:s');
                $records->image_map = $filename;
                $records->lat = $request->input('lat');
                $records->lon = $request->input('lon');
                $records->save();

                return redirect()->to('/records')->with('success', 'Record On Meeting Added');

            }
            else{

                $image_map = $request->input('image_map');
                $filename = 'onmeeting-' . time() . '.' . 'png';
                Image::make($image_map)->resize(750,750)->save( public_path('/uploads/locations/' . $filename) );

                $user = Auth::user();
                $records = Records::where('user_id','=',$user->id)->whereDate('created_at','=',date('Y-m-d'))->first();

                $records->statuslog = 'on meeting';
                $records->updated_at = date('Y-m-d H:m:s');
                $records->image_map = $filename;
                $records->lat = $request->input('lat');
                $records->lon = $request->input('lon');
                $records->save();

                return redirect()->to('/records')->with('success', 'Record Updated On Meeting');  
            }   
    	}
    	else if($request->has('absent')){

            $result = Records::where('user_id','=',Auth::user()->id)->whereDate('created_at','=',date('Y-m-d'))->get();

            if(count($result) <= 0){

                $image_map = $request->input('image_map');
                $filename = 'absent-' . time() . '.' . 'png';
                Image::make($image_map)->resize(750,750)->save( public_path('/uploads/locations/' . $filename) );

                $records = new Records;
                $records->login = null;
                $records->logout = null;
                $records->statuslog = 'absent';
                $records->leavelog = null;
                $records->user_id = Auth::user()->id;
                $records->created_at = date('Y-m-d H:m:s');
                $records->updated_at = date('Y-m-d H:m:s');
                $records->image_map = $filename;
                $records->lat = $request->input('lat');
                $records->lon = $request->input('lon');
                $records->save();

                return redirect()->to('/records')->with('success', 'Record Absent Added');
            }
            else{

                $image_map = $request->input('image_map');
                $filename = 'absent-' . time() . '.' . 'png';
                Image::make($image_map)->resize(750,750)->save( public_path('/uploads/locations/' . $filename) );

                $user = Auth::user();
                $records = Records::where('user_id','=',$user->id)->whereDate('created_at','=',date('Y-m-d'))->first();

                $records->statuslog = 'absent';
                $records->updated_at = date('Y-m-d H:m:s');
                $records->image_map = $filename;
                $records->lat = $request->input('lat');
                $records->lon = $request->input('lon');
                $records->save();

                return redirect()->to('/records')->with('success', 'Record Updated Absent');

            }
                   
    	}
    	else{

    		return redirect()->to('/home');
    	}
    }

    public function ShowRecords(){

        if(Auth::user()->privilege == 'Admin'){

            $records = Records::with('Users')->where('user_id','!=',Auth::user()->id)->orderBy('id', 'desc')->paginate(15);

            return view('records.employeerecords', ['records' => $records]);
        }
        else{

            return redirect()->to('/home');
        }
    }

    public function SortRecords(Request $request){

        if(Auth::user()->privilege == 'Admin'){
           
           if($request->input('search') != ""){

            $search = $request->input('search');

                if($request->input('recordset') == 'year'){

                    $records = Records::whereHas('Users', function($query) use($search){
                        $query->where('name', 'like', '%'.$search.'%');
                        })->whereYear('created_at', '=', date('Y'))
                        ->paginate(15);
                
                    return view('records.employeerecords', ['records'=>$records]);
                }
                else if($request->input('recordset') == 'month'){

                    $records = Records::whereHas('Users', function($query) use($search){
                        $query->where('name', 'like', '%'.$search.'%');
                    })->whereMonth('created_at','=', date('m'))->paginate(15);

                    return view('records.employeerecords', ['records'=>$records]);
                }
                else{

                    $records = Records::whereHas('Users', function($query) use($search){
                        $query->where('name', 'like', '%'.$search.'%');
                    })->whereDate('created_at', '=', date('Y-m-d'))->paginate(15);

                    return view('records.employeerecords', ['records'=>$records]);
                }
           }
           else{

                if($request->input('recordset') == 'year'){

                    $records = Records::where('user_id','!=',Auth::user()->id)->whereYear('created_at', '=', date('Y'))->paginate(15);
                
                    return view('records.employeerecords', ['records'=>$records]);
                }
                else if($request->input('recordset') == 'month'){

                    $records = Records::where('user_id','!=',Auth::user()->id)->whereMonth('created_at','=', date('m'))->paginate(15);

                    return view('records.employeerecords', ['records'=>$records]);
                }
                else{

                    $records = Records::where('user_id','!=',Auth::user()->id)->whereDate('created_at', '=', date('Y-m-d'))->paginate(15);

                    return view('records.employeerecords', ['records'=>$records]);
                }
            }
        }
        else{

            return redirect()->to('/home');
        }
    }
}
