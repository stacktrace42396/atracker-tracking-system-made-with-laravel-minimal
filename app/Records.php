<?php

namespace AttendanceTracker;

use Illuminate\Database\Eloquent\Model;

class Records extends Model
{
    protected $table = 'records';

    public function Users(){

    	return $this->belongsTo('AttendanceTracker\Users', 'user_id');
    }
}
