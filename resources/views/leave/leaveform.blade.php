@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Form: On Leave</div>

                <div class="panel-body">
                @include('layouts.success')
                @include('layouts.errors')
                @include('layouts.error')
          			
          			<form method="post" action="{{ route('setleave') }}">
          				{{ csrf_field() }}
                        <input type="hidden" name="image_map" id="image_map">
                        <input type="hidden" name="lat" id="lat">
                        <input type="hidden" name="lon" id="lon">
	                	<fieldset class="form-group">
	                		<label for="datefrom">From:</label>
	                		<input type="date" class="form-control" id="datefrom" name="datefrom" value="{{ old('datefrom') }}">
	                	</fieldset>
	                	<fieldset class="form-group">
	                		<label for="dateto">To:</label>
	                		<input type="date" class="form-control" id="dateto" name="dateto" value="{{ old('dateto') }}">
	                	</fieldset>
	                	<fieldset class="form-group">
	                		<input type="submit" class="form-control btn-primary" value="Submit">
	                	</fieldset>
	                	<fieldset class="form-group">
	                		<input type="reset" class="form-control btn-primary" value="Reset">
	                	</fieldset>
                	</form>

          		</div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else { 
            alert('Geolocation Not Supported');
        }
    }

    function showPosition(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var user_position = latitude+','+longitude;

        var image_map = "http://maps.googleapis.com/maps/api/staticmap?center="
                         +user_position+"&markers=color:green%7C"+latitude+","+longitude+"&zoom=15&size=750x750&key=AIzaSyDW55EYCizkIYx8_xm8-gyq4CilW5gsBYc";

        document.getElementById("image_map").value = image_map;
        document.getElementById("lat").value = latitude;
        document.getElementById("lon").value = longitude;          
    }

    function showError(error) {
        switch(error.code) {
            case error.PERMISSION_DENIED:
                alert('User denied the request for Geolocation.');
                break;
            case error.POSITION_UNAVAILABLE:
                alert('Location information is unavailable.');
                break;
            case error.TIMEOUT:
                alert('The request to get user location timed out.');
                break;
            case error.UNKNOWN_ERROR:
                alert('An unknown error occurred.');
                break;
        }
    }

    getLocation();
</script>
@endsection