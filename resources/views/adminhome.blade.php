@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ Auth::user()->privilege }} Dashboard</div>

                <div class="panel-body">
                    @include('layouts.success')
                    @include('layouts.errors')
                    @include('layouts.error')

                    <a class="btn btn-link" href="{{ route('register') }}">Register An Employee</a>
                    <hr>
                    <a class="btn btn-link" href="{{ route('accountadmin', Auth::user()->position) }}">Employee Profiles</a>
                    <hr>
                    <a class="btn btn-link" href="{{ route('showrecords') }}">My Records</a>
                    <hr>
                    <a class="btn btn-link" href="{{ route('showemployeerecords') }}">Attendance Records</a>
                    <hr>
                    <a class="btn btn-link" href="{{ route('editadminaccount') }}">Edit Account</a>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Attendance For Today</div>

                <div class="panel-body">
                    
                    <form action="{{ route('setattendance') }}" method="post" class="form-group">
                        {{ csrf_field() }}
                        <input type="hidden" name="image_map" id="image_map">
                        <input type="hidden" name="lat" id="lat">
                        <input type="hidden" name="lon" id="lon">
                        <p>
                            <input type="submit" value="Time In" name="timein" id="timein" class="btn btn-primary form-control">
                        </p>
                        <p>
                            <input type="submit" value="Time Out" name="timeout" id="timeout" class="btn btn-primary form-control">
                        </p>
                        <p>
                            <input type="submit" value="Work From Home" name="workfromhome" id="workfromhome" class="btn btn-primary form-control">
                        </p>
                        <p>
                             <input type="submit" value="On Meeting" name="onmeeting" id="onmeeting" class="btn btn-primary form-control">
                        </p>
                        <p>
                            <input type="submit" value="Absent" name="absent" id="absent" class="btn btn-primary form-control">
                        </p> 
                    </form>

                    <a href="{{ route('leaveform') }}" class="btn btn-primary form-control">On Leave</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else { 
            alert('Geolocation Not Supported');
        }
    }

    function showPosition(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var user_position = latitude+','+longitude;

        var image_map = "http://maps.googleapis.com/maps/api/staticmap?center="
                         +user_position+"&markers=color:green%7C"+latitude+","+longitude+"&zoom=15&size=750x750&key=AIzaSyDW55EYCizkIYx8_xm8-gyq4CilW5gsBYc";

        document.getElementById("image_map").value = image_map;  
        document.getElementById("lat").value = latitude;
        document.getElementById("lon").value = longitude;          
    }

    function showError(error) {
        switch(error.code) {
            case error.PERMISSION_DENIED:
                alert('User denied the request for Geolocation.');
                break;
            case error.POSITION_UNAVAILABLE:
                alert('Location information is unavailable.');
                break;
            case error.TIMEOUT:
                alert('The request to get user location timed out.');
                break;
            case error.UNKNOWN_ERROR:
                alert('An unknown error occurred.');
                break;
        }
    }

    getLocation();
</script>
@endsection
