@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ Auth::user()->privilege }} Dashboard</div>

                <div class="panel-body">
                    <a class="btn btn-link" href="{{ route('register') }}">Register An Employee</a>
                    <hr>
                    <a class="btn btn-link" href="{{ route('accountmaster') }}">Employee Accounts</a>
                    <hr>
                    <a class="btn btn-link" href="{{ route('showallattendance') }}">Attendance Records</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection