@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
                            
        @include('layouts.success')
        @include('layouts.errors')
        @include('layouts.error')

        @if( $records->isEmpty() )
			<h1>No Records</h1>

		@else
			<h1>{{ Auth::user()->position }} Attendance Records:</h1>
			<hr>
			
			<form method="post" action="{{ route('sortfromadmin') }}">
				{{ csrf_field() }}
				<fieldset class="form-group">
					<label for="search">Search: </label>
					<input type="text" name="search" id="search" value="{{ old('search') }}" class="form-text">

					<label for="recordset">Set Records:</label>
					<select name="recordset" id="recordset" class="form-group">
						<option value="day">By This Day</option>
						<option value="month">By This Month</option>
						<option value="year">By This Year</option>
					</select>

					<button type="submit" class="btn btn-md btn-primary">Search</button>
				</fieldset>
			</form>

			<table class="table table-responsive">
				
				<tr>
					<th class="text-center"><p>Name</p></th>
					<th class="text-center"><p>Position</p></th>
					<th class="text-center"><p>Status Log</p></th>
					<th class="text-center"><p>Login</p></th>
					<th class="text-center"><p>Logout</p></th>
							
				</tr>
				@foreach($records as $record)
				<tr>
					@if($record->users->position == Auth::user()->position)
						<td class="text-center">{{ $record->users->name }}</td>
						<td class="text-center">{{ $record->users->position }}</td>
						<td class="text-center">{{ $record->statuslog }}</td>
						@if($record->login == "")
							<td class="text-center">No Login</td>
						@else
							<td class="text-center">{{ date("D M-d-Y g:i:s A",strtotime($record->login)) }}</td>
						@endif
						@if($record->logout == "")
							<td class="text-center">No Logout</td>
						@else
							<td class="text-center">{{ date("D M-d-Y g:i:s A",strtotime($record->logout)) }}</td>
						@endif
					@else
					@endif		
				</tr>
				@endforeach
			</table>
		@endif

		<div class="text-center">
            {{ $records->links() }}
        </div>

    </div>
</div>
@endsection