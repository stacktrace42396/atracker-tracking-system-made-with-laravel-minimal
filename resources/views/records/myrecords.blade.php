@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
                            
        @include('layouts.success')
        @include('layouts.errors')
        @include('layouts.error')

        @if( $records->isEmpty() )
					<h1>No Records</h1>
					@else
					<h1>My Records:</h1>
					<hr>
					
						<table class="table table-responsive">
						
						<tr>
							<th><p>Name</p></th>
							<th><p>Status Log</p></th>
							<th><p>Login</p></th>
							<th><p>Logout</p></th>
							
						</tr>
						@foreach($records as $record)
						<tr>
							<td style="vertical-align: middle;"><p>{{ $record->users->name }}</p></td>
							<td style="vertical-align: middle;"><p>{{ strtoupper($record->statuslog) }}</p></td>
							@if($record->login == "")
								<td style="vertical-align: middle;"><p>No Login</p></td>
							@else
								<td style="vertical-align: middle;"><p>{{ date("D M-d-Y g:i:s A",time($record->login)) }}</p></td>
							@endif
							
							@if($record->logout == "")
								<td style="vertical-align: middle;"><p>No Logout</p></td>
							@else
								<td style="vertical-align: middle;"><p>{{ date("D M-d-Y g:i:s A",time($record->logout)) }}</p></td>
							@endif
						</tr>
						@endforeach
					</table>
					@endif

					<div class="text-center">
                    	{{ $records->links() }}
                	</div>

    </div>
</div>
@endsection