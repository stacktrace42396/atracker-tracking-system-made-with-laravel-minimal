@extends('layouts.app')

@section('content')
	
<div class="container">
    <div class="row">
                    
					@if( $users->isEmpty() )
					<h1>No Values</h1>
					@else
						<h1>{{ Auth::user()->position }} Accounts</h1>
						<hr>

						<table class="table">
						<tr>
							<th class="text-center"><p>Name</p></th>
							<th class="text-center"><p>Email</p></th>
							<th class="text-center"><p>Privilege</p></th>
							<th class="text-center"><p>Options</p></th>
						</tr>
						@foreach($users as $user)
						<tr>
							<td style="vertical-align: middle;"><p class="text-center">{{ $user->name }}</p></td>
							<td style="vertical-align: middle;"><p class="text-center">{{ $user->email }}</p></td>
							<td style="vertical-align: middle;"><p class="text-center">{{ $user->privilege }}</p></td>
							<td><a href="{{ route('viewaccount', ['id'=>$user->id]) }}" class="btn btn-primary form-control">View Profile</a></td>
						</tr>
						@endforeach
					</table>
					@endif
                
                <div class="text-center">
                	{{ $users->links() }}
                </div>
                

    </div>
</div>

@endsection