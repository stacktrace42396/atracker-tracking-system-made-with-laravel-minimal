@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ Auth::user()->name }}'s Account</div>
				<div class="panel-body">

				@include('layouts.success')
				@include('layouts.error')
                @include('layouts.errors')
                    
                    <form enctype="multipart/form-data" action="{{ route('editadminnow') }}" method="post">
                    {{ csrf_field() }}

                        <fieldset class="group-horizontal text-center">
                            <img src="/uploads/avatars/{{ Auth::user()->avatar }}" class="img img-circle" height="150px" width="150px"> 
                        </fieldset>

                    	<fieldset class="form-group text-center">
                    		<label for="avatar">Profile Picture</label>
                    		<input type="file" name="avatar" class="form-control">
                    	</fieldset>

                        <fieldset class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ Auth::user()->name }}">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ Auth::user()->email }}">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="position">Position</label>
                            <select name="position" id="position" class="form-control">
                                @foreach($pos as $po)
                                    @if(Auth::user()->position == $po)
                                        <option value="{{$po}}" selected>{{$po}}</option>
                                    @else
                                        <option value="{{$po}}">{{$po}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="privilege">Privilege</label>
                            <select name="privilege" id="privilege" class="form-control">
                                    <option value=""></option>
                                    <option value="{{ Auth::user()->privilege }}" selected>{{ Auth::user()->privilege }}</option>
                            </select>
                        </fieldset>

                    	<fieldset class="pull-right btn-group-horizontal">
                    		<input type="submit" value="Change" class="btn btn-primary">
                    		<input type="reset" value="Reset" class="btn btn-primary">
                    	</fieldset>
                    </form>

                </div>     
            </div>
        </div>
    </div>
</div>

@endsection