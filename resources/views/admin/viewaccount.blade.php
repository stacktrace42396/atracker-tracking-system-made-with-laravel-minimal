@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">View Account</div>
				
				<div class="panel-body">
                    
                    <div class="text-center">
                        <img src="/uploads/avatars/{{ $user->avatar }}" class="img img-circle" height="150px" width="150px">
                        <h1 class="text-primary text-center">{{ $user->name }}'s Profile</h1>                        
                    </div>
                    <hr>

                    <div class="align-content-center">
                        <h3 class="text-center">Email: {{ $user->email }}</h3>
                        <hr>
                        <h3 class="text-center">Position: {{ $user->position }}</h3>
                        <hr>
                        <h3 class="text-center">Privilege: {{ $user->privilege }}</h3>
                    </div>

                </div>
                
            </div>
        </div>
    </div>
</div>

@endsection