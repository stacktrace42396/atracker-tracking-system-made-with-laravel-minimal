@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
				
					@if( $users->isEmpty() )
					<h1>No Employees</h1>
					@else
						<h1>All Employees</h1>
						<hr>
						<table class="table">
						@include('layouts.success')
						<tr>
							<th><p>Name</p></th>
							<th><p>Email</p></th>
							<th><p>Position</p></th>
							<th><p>Privilege</p></th>
							<th class="text-center">Options</th>
						</tr>
						@foreach($users as $user)
						<tr>
							<td style="vertical-align: middle;"><p>{{ $user->name }}</p></td>
							<td style="vertical-align: middle;"><p>{{ $user->email }}</p></td>
							<td style="vertical-align: middle;"><p>{{ $user->position }}</p></td>
							<td style="vertical-align: middle;"><p>{{ $user->privilege }}</p></td>
							<td><p><a href="{{ route('editaccount', ['id'=>$user->id]) }}" class="btn btn-primary form-control">Edit</a></p>
							<p><a href="{{ route('deleteaccount', ['id'=>$user->id]) }}" class="btn btn-primary form-control">Delete</a></p></td>
						</tr>
						@endforeach
					</table>
					@endif
                    
                <div class="text-center">
                    {{ $users->links() }}
                </div>
                
    </div>
</div>

@endsection