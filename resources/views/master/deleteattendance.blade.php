@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Delete Attendance</div>
				
				<div class="panel-body">
                    
                    <div class="alert alert-warning" role="alert">
                        <strong>Warning: </strong>
                        Are you sure you want to delete <strong>Record {{ $record->id }}?</strong>?
                    </div>
                    <div class="btn-group-horizontal text-center">
                        <form action="{{ route('setdeleterecord', ['id'=>$record->id]) }}" method="post">
                            {{ csrf_field() }}
                            <input type="submit" value="Yes" class="btn btn-primary">
                            <a class="btn btn-primary" href="#" role="button">No</a>
                        </form>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>

@endsection