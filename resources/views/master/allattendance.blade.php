@extends('layouts.app')

@section('content')

<div class="container">
    
        <div class="row">
            	
				@if( $records->isEmpty() )
					<h1>No Attendance Records</h1>
				@else
					<h1>Attendance Records:</h1>
					<hr>

					@if( session()->has('error') )
						<div class="alert alert-danger">
							{{ session('error') }}
						</div>
					@endif

					<form method="post" action="{{ route('sortfrommaster') }}">
						{{ csrf_field() }}
						<fieldset class="form-group">
							<label for="search">Search: </label>
							<input type="text" name="search" id="search" value="{{ old('search') }}" class="form-text">

							<label for="recordset">Set Records:</label>
							<select name="recordset" id="recordset" class="form-group">
								<option value="day">By This Day</option>
								<option value="month">By This Month</option>
								<option value="year">By This Year</option>
							</select>

							<button type="submit" class="btn btn-md btn-primary">Search</button>
						</fieldset>
					</form>
					
					<table class="table table-responsive">
					@include('layouts.success')
						<tr>
							<th><p>Name</p></th>
							<th><p>Status Log</p></th>
							<th><p>Login</p></th>
							<th><p>Logout</p></th>
							<th><p>Update Time</p></th>
							<th class="text-center">Options</th>
							
						</tr>
						@foreach($records as $record)
						<tr>
							<td style="vertical-align: middle;"><p>{{ $record->users->name }}</p></td>
							
							<td style="vertical-align: middle;"><p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#location">{{ strtoupper($record->statuslog) }}</button></p></td>
							
							@if($record->login == "")
								<td style="vertical-align: middle;"><p>No Login</p></td>
							@else
								<td style="vertical-align: middle;"><p>{{ date("D M-d-Y g:i:s A",strtotime($record->login)) }}</p></td>
							@endif
							
							@if($record->logout == "")
								<td style="vertical-align: middle;"><p>No Logout</p></td>
							@else
								<td style="vertical-align: middle;"><p>{{ date("D M-d-Y g:i:s A",strtotime($record->logout)) }}</p></td>
							@endif
							
							<td style="vertical-align: middle;"><p>{{ date("D M-d-Y g:i:s A",strtotime($record->updated_at)) }}</p></td>
							
							<td style="vertical-align: middle;"><p><a href="{{ route('showeditrecord', ['id'=>$record->id]) }}" class="btn btn-primary form-control">Edit</a></p>
							<a href="{{ route('showdeleterecord', ['id'=>$record->id]) }}" class="btn btn-danger form-control">Delete</a></td>
						</tr>
						@endforeach
				@endif	
					</table>
					
					@foreach($records as $record)
					<div id="location" class="modal fade" role="dialog" aria-hidden="true">
  						<div class="modal-dialog">
    						<div class="modal-content">
        						<div class="modal-body">
            						<img src="/uploads/locations/{{ $record->image_map }}" class="img-responsive">
            						<p>
            							<h4><strong>Latitude :</strong>{{ $record->lat }}</h4>
            						</p>
            						<p>
            							<h4><strong>Longitude :</strong>{{ $record->lon }}</h4>
            						</p> 
        						</div>
    						</div>
  						</div>
					</div>
					@endforeach

					<div class="text-center">
                    	{{ $records->links() }}
                	</div>

        </div>
</div>
@endsection