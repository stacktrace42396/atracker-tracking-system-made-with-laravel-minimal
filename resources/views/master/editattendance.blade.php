@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Record</div>
				
				<div class="panel-body">

				@include('layouts.success')
				@include('layouts.errors')
				@include('layouts.error')

				<form method="post" action="{{ route('seteditrecord', ['id'=>$record->id]) }}">
				{{ csrf_field() }}
					<fieldset class="form-group">
						<label for="status">Log Status</label>
						<select name="status" id="status" class="form-control">
							@foreach($status as $stat)
								@if($stat == $record->statuslog)
									<option value="{{ $stat }}" selected>{{$stat}}</option>
								@else
									<option value="{{ $stat }}">{{$stat}}</option>
								@endif
							@endforeach
						</select>
					</fieldset>
					<fieldset class="form-group">
						<label for="login">Login Time</label>
						<input type="text" class="form-control" id="login" name="login" value="{{$record->login}}">
					</fieldset>
					<fieldset class="form-group">
						<label for="logout">Logout Time</label>
						<input type="text" class="form-control" id="logout" name="logout" value="{{$record->logout}}">
					</fieldset>
					<fieldset class="btn-group-horizontal text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-primary">Reset</button>
                    </fieldset>
				</form>

          		</div>
                
            </div>
        </div>
    </div>
</div>
@endsection