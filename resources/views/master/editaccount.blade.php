@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Account</div>

				<div class="panel-body">

                @include('layouts.success')
                @include('layouts.error')
                @include('layouts.errors')
                    
                    <form enctype="multipart/form-data" method="post" action="{{ route('editaccountnow', ['id'=>$user->id]) }}">
                        {{ csrf_field() }}
                        
                        <fieldset class="group-horizontal text-center">
                            <img src="/uploads/avatars/{{ $user->avatar }}" class="img img-circle" height="150px" width="150px"> 
                        </fieldset>    

                        <fieldset class="form-group text-center">
                            <label for="avatar">Profile Picture</label>
                            <input type="file" name="avatar" class="form-control">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="position">Position</label>
                            <select name="position" id="position" class="form-control">
                                @foreach($pos as $po)
                                    @if($user->position == $po)
                                        <option value="{{$po}}" selected>{{$po}}</option>
                                    @else
                                        <option value="{{$po}}">{{$po}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="privilege">Privilege</label>
                            <select name="privilege" id="privilege" class="form-control">
                                    @foreach($priv as $pri)
                                    @if($user->privilege == $pri)
                                        <option value="{{$pri}}" selected>{{$pri}}</option>
                                    @else
                                        <option value="{{$pri}}">{{$pri}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </fieldset>
                        <fieldset class="btn-group-horizontal text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-primary">Reset</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection