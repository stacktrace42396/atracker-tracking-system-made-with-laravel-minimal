<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
	if(Auth::check()){
		return redirect()->route('homepage');
	}
	else{
		return view('auth.login');
	}
});

Auth::routes();

Route::name('homepage')->get('/home', 'HomeController@index');

//	Admin
Route::name('accountadmin')->get('/accounts/{position}', 'AdminController@ShowAccounts');
Route::name('viewaccount')->get('/accounts/view/{id}', 'AdminController@ViewAccount');
Route::name('editadminaccount')->get('/admin/edit', 'AdminController@EditAccount');
Route::name('editadminnow')->post('/admin/edit', 'AdminController@UpdateAccount');

//	Client
Route::name('vieweditclient')->get('/client/edit', 'ClientController@ViewEditAccount');
Route::name('editclient')->post('/client/edit', 'ClientController@EditAccount');

//	Master
Route::name('accountmaster')->get('/accounts', 'MasterController@ShowAccounts');
Route::name('editaccount')->get('/accounts/edit/{id}', 'MasterController@ViewEditAccount');
Route::name('editaccountnow')->post('/accounts/edit/{id}', 'MasterController@EditAccount');
Route::name('deleteaccount')->get('/accounts/delete/{id}', 'MasterController@ViewDeleteAccount');
Route::name('deleteaccountnow')->post('/accounts/delete/{id}', 'MasterController@DeleteAccount');

//	Records
Route::name('showrecords')->get('/records', 'RecordsController@index');
Route::name('setattendance')->post('/records', 'RecordsController@set');
Route::name('showemployeerecords')->get('/records/employees', 'RecordsController@ShowRecords');
Route::name('sortfromadmin')->post('/records/employees', 'RecordsController@SortRecords');

//	Leave
Route::name('leaveform')->get('/leave', 'LeaveController@index');
Route::name('setleave')->post('/leave', 'LeaveController@set');
Route::name('showallattendance')->get('/attendance/all', 'MasterController@ShowRecords');
Route::name('sortfrommaster')->post('/attendance/all', 'MasterController@SortRecords');
Route::name('showeditrecord')->get('/attendance/edit/{id}', 'MasterController@ShowEditRecords');
Route::name('seteditrecord')->post('/attendance/edit/{id}', 'MasterController@SetEditRecords');
Route::name('showdeleterecord')->get('/attendance/delete/{id}', 'MasterController@ShowDeleteRecord');
Route::name('setdeleterecord')->post('/attendance/delete/{id}', 'MasterController@SetDeleteRecord');